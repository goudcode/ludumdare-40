﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DoorManager : MonoBehaviour {

    private List<Door> doors;

	void Start () {
        doors = FindObjectsOfType<Door>().ToList();
	}

    public void UnlockNextDoor()
    {
        int lowestDoorNumber = 10000;
        if(doors.Where(x => x.isLocked).Count() > 0)
        {
            foreach (Door door in doors.Where(x => x.isLocked))
            {
                if (door.doorNumber < lowestDoorNumber)
                    lowestDoorNumber = door.doorNumber;
            }

            Door doorToUnlock = doors.Find(x => x.doorNumber == lowestDoorNumber);
            if(doorToUnlock)
            {
                doorToUnlock.OpenDoor();
            }
        }
        else
        {
            Debug.Log("No more doors to unlock");
        }
    }
}
