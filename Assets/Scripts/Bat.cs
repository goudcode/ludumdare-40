﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Bat : MonoBehaviour
{
    public enum BatState
    {
        Idle,
        Pray,
        Attack,
        Return
    }

    private BatState state;
    private Animator animator;
    private AudioSource audioSource;
    private DateTime attackOn;
    private Vector3 startPosition;
    private bool isPlayingSound = false;
    private bool playSound = false;
    private GameObject player;

    public int sightDistance = 10;
    public int seccondsBeforeAttack = 3;
    public float speed = 2f;
    public int millesecondsBetweenSound = 75;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        startPosition = transform.position;
    }

    void Update()
    {
        switch (state)
        {
            case BatState.Idle:
                OnIdle();
                break;

            case BatState.Pray:
                OnPray();
                break;

            case BatState.Attack:
                OnAttack();
                break;

            case BatState.Return:
                OnReturn();
                break;
        }

        // Enable sound
        if (playSound && !isPlayingSound)
            StartCoroutine(SoundLoop());
    }

    private void OnReturn()
    {
        if (PlayerInSight())
        {
            state = BatState.Attack;
            return;
        }

        MoveTo(startPosition);
        if (transform.position == startPosition)
        {
            animator.SetBool("IsFlying", false);
            state = BatState.Idle;
            playSound = false;
            return;
        }
    }

    private void OnAttack()
    {
        if (!PlayerInSight())
        {
            state = BatState.Return;
            return;
        }

        MoveTo(player.transform.position);
    }

    void OnIdle()
    {
        //Find player
        if (!PlayerInSight())
            return;

        playSound = true;
        animator.SetBool("IsFlying", true);
        state = BatState.Pray;
        attackOn = DateTime.Now.AddSeconds(seccondsBeforeAttack);
    }

    void OnPray()
    {
        if (!PlayerInSight())
        {
            animator.SetBool("IsFlying", false);
            playSound = false;
            state = BatState.Idle;
            return;
        }

        if (DateTime.Now >= attackOn)
            state = BatState.Attack;
    }

    private void MoveTo(Vector3 target)
    {
        transform.position = Vector3.MoveTowards(
            transform.position, target, (speed * Time.deltaTime));
    }

    private bool PlayerInSight()
    {
        //Find player
        if (player == null)
            return false;

        //See if player is in sight
        RaycastHit2D hit = Physics2D.Linecast(transform.position, player.transform.position,
            ~(1 << gameObject.layer));
        if (hit.collider?.tag == "Player")
        {
            var distance = (transform.position - hit.collider.transform.position).magnitude;
            return distance < sightDistance;
        }

        return false;
    }

    private IEnumerator SoundLoop()
    {
        isPlayingSound = true;

        while (playSound)
        {
            yield return new WaitForSeconds((float)millesecondsBetweenSound / 1000);
            if (playSound)
            {
                audioSource.Play();
            }
        }

        isPlayingSound = false;
        yield return null;
    }
}
