﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Range(1, 10)]
    public float jumpVelocity;

    [Range(1, 10)]
    public float fallMultiplier = 2.5f;

    [Range(1, 10)]
    public float lowJumpMultiplier = 2f;

    [Range(0.5f, 1.5f)]
    public float hurtTime = .5f;

    [Range(.1f, .5f)]
    public float blinkInterval = .1f;

    [Range(1, 10)]
    public int startingHp = 3;

    public GameObject lantern;
    public LayerMask groundLayer;
    public Transform head;
    public float maxSpeed = 5f;
    private float currentSpeed;
    private int currentHp;

    public AudioClip hurtSound;
    public AudioClip throwSound;
    public AudioClip jumpSound;
    public AudioClip pickupSound;

    private Animator animator;
    private AudioSource audioSource;
    private SpriteRenderer spriteRenderer;
    private Vector2 velocity;
    private Rigidbody2D rigid2d;
    private Light lanternLight;
    private Shop shop;
    private float lanternBrightness;
    private bool grounded = true;
    private bool beingHurt = false;
    private Stack<GameObject> stackedTreasure;
    public int AmountOfTreasure { get { return stackedTreasure.Count; } }
    private bool facingRight = true;
    private bool canMove = true;

    void Start()
    {
        stackedTreasure = new Stack<GameObject>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        rigid2d = GetComponent<Rigidbody2D>();
        lanternLight = lantern.GetComponentInChildren<Light>();
        shop = FindObjectOfType<Shop>();
        lanternBrightness = GameManager.Instance.options.lanternBrightness;
        currentHp = startingHp;
        UIController.Instance.UpdateHealth(currentHp, startingHp);
    }

    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.F1))
        {
            jumpVelocity = 20;
            maxSpeed = 20;
        }
#endif

        float movement = Input.GetAxisRaw("Horizontal");
        currentSpeed = maxSpeed - (stackedTreasure.Count * GameManager.Instance.options.treasureWeight);
        lanternLight.range = lanternBrightness + 5;

        SetGrounded(IsGrounded());

        if (Input.GetKeyDown(KeyCode.Q))
        {
            ThrowTreasure(false);
        }

        if (!canMove)
            return;

        if (movement > 0)
        {
            facingRight = true;
            animator.SetBool("IsWalking", true);
            spriteRenderer.flipX = false;
            lantern.transform.localPosition = new Vector3(0.1f, 0, 0);
            transform.Translate(new Vector3(currentSpeed * Time.deltaTime, 0, 0));
        }
        else if (movement < 0)
        {
            facingRight = false;
            animator.SetBool("IsWalking", true);
            spriteRenderer.flipX = true;
            lantern.transform.localPosition = new Vector3(-0.1f, 0, 0);
            transform.Translate(new Vector3(-currentSpeed * Time.deltaTime, 0, 0));
        }
        else
            animator.SetBool("IsWalking", false);

        var jumpModifier = 1 - (((float)stackedTreasure.Count / 5) * .35f);
        if (Input.GetButton("Jump") && grounded)
        {
            rigid2d.velocity = Vector2.up * jumpVelocity * jumpModifier;
            audioSource.PlayOneShot(jumpSound);
        }

        if (rigid2d.velocity.y < 0)
        {
            rigid2d.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (rigid2d.velocity.y > 0 && !Input.GetButton("Jump"))
        {
            rigid2d.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }
    }

    public void DestroyTreasure()
    {
        if (stackedTreasure.Count > 0)
        {
            var go = stackedTreasure.Pop();
            Destroy(go);
        }
    }

    public void PickUpTreasure(GameObject go)
    {
        if (!stackedTreasure.Contains(go))
        {
            var rb = go.GetComponent<Rigidbody2D>();
            audioSource.PlayOneShot(pickupSound);

            go.transform.SetParent(head);
            go.transform.localPosition = new Vector3(0, (0 + (stackedTreasure.Count * 0.1f)), 0);
            go.GetComponent<Collider2D>().isTrigger = true;

            stackedTreasure.Push(go);
            rb.freezeRotation = true;
            go.transform.rotation = Quaternion.identity;
            rb.velocity = Vector2.zero;
            rb.isKinematic = true;
        }
    }

    private void ThrowTreasure(bool randomDirection)
    {
        if (stackedTreasure.Count > 0)
        {
            var go = stackedTreasure.Pop();
            var rb = go.GetComponent<Rigidbody2D>();
            audioSource.PlayOneShot(throwSound);

            go.transform.parent = null;
            go.GetComponent<Collider2D>().isTrigger = false;

            Vector2 direction = randomDirection ?
                new Vector2(UnityEngine.Random.Range(-.7f, .7f), UnityEngine.Random.Range(0.5f, 1f)) :
                facingRight ? new Vector2(.7f, 1) :
                new Vector2(-.7f, 1);

            rb.freezeRotation = false;
            rb.bodyType = RigidbodyType2D.Dynamic;
            rb.AddForce(direction * 8f, ForceMode2D.Impulse);
        }
    }

    private void SetGrounded(bool value)
    {
        grounded = value;
        animator.SetBool("IsGrounded", value);
    }

    public void ResetLanternBrightness()
    {
        lanternBrightness = GameManager.Instance.options.lanternBrightness;
    }

    private bool IsGrounded()
    {
        var groundCheck = Physics2D.Raycast(transform.position + new Vector3(0.2f, 0, 0), Vector2.down, .6f, groundLayer);
        var groundCheck1 = Physics2D.Raycast(transform.position - new Vector3(0.2f, 0, 0), Vector2.down, .6f, groundLayer);
        return groundCheck.collider != null || groundCheck1.collider != null;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Danger")
        {
            if (!beingHurt)
                StartCoroutine(BeingHurt());
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        switch (other.gameObject.tag)
        {
            case "Shop":
                shop.OpenShop();
                break;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Shop")
            shop.CloseShop();
    }

    public IEnumerator BeingHurt()
    {
        beingHurt = true;
        var endHurt = DateTime.Now.AddSeconds(hurtTime);
        audioSource.PlayOneShot(hurtSound);

        currentHp--;
        UIController.Instance.UpdateHealth(currentHp, startingHp);
        if (currentHp <= 0 && canMove)
        {
            Die();
        }

        while (DateTime.Now < endHurt)
        {
            yield return new WaitForSeconds(blinkInterval);
            spriteRenderer.color = new Color(
                spriteRenderer.color.r,
                spriteRenderer.color.g,
                spriteRenderer.color.b,
                spriteRenderer.color.a == 1f ? .5f : 1f);
        }

        spriteRenderer.color = new Color(
                spriteRenderer.color.r,
                spriteRenderer.color.g,
                spriteRenderer.color.b,
                1);
        beingHurt = false;

        yield return null;
    }

    private void DropAllTreasure()
    {
        var count = stackedTreasure.Count;
        for (int i = 0; i < count; ++i)
            ThrowTreasure(true);
    }

    public void Die()
    {
        canMove = false;
        DropAllTreasure();
        transform.position = GameManager.Instance.lastCheckpoint;
        canMove = true;
        ResetHealth();
    }

    public int GetCurrentHp()
    {
        return currentHp;
    }

    public void ResetHealth()
    {
        currentHp = startingHp;
        UIController.Instance.UpdateHealth(currentHp, startingHp);
    }

    public bool HasGem()
    {
        foreach (var treasure in stackedTreasure)
            if (treasure.GetComponent<Treasure>().type == Treasure.TreasureType.Gem)
                return true;
        return false;
    }
}
