﻿using UnityEngine;

public class Treasure : MonoBehaviour
{
    public enum TreasureType
    {
        Normal,
        Gem
    }

    private GameObject player;
    private PlayerController playercontroller;

    public TreasureType type;

    private void Start()
    {
        playercontroller = FindObjectOfType<PlayerController>();
        player = playercontroller.gameObject;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (Vector3.Distance(player.transform.position, transform.position) < 2)
            {
                playercontroller.PickUpTreasure(gameObject);
            }
        }
    }
}