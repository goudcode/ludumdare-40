﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Shop : MonoBehaviour
{
    private bool shopOpen = false;
    private DoorManager doorManager;
    private PlayerController playerController;

    void Start()
    {
        doorManager = FindObjectOfType<DoorManager>();
        playerController = FindObjectOfType<PlayerController>();
    }

    void Update()
    {
        if (shopOpen)
        {
            UIController.Instance.unlockDoorIndicator.transform.position = Camera.main.WorldToScreenPoint(transform.position + new Vector3(-.75f, 2f, 0));
            if (Input.GetKeyDown(KeyCode.G))
            {
                if (playerController.HasGem())
                    SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
                else if (playerController.AmountOfTreasure >= 5)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        playerController.DestroyTreasure();
                    }
                    UIController.Instance.unlockDoorIndicator.GetComponent<Text>().text = "You have unlocked the next area!!";
                    doorManager.UnlockNextDoor();
                }
                else
                {
                    UIController.Instance.unlockDoorIndicator.GetComponent<Text>().text = "You don't have enough treasure!!";
                }
            }
        }
    }

    public void OpenShop()
    {
        UIController.Instance.unlockDoorIndicator.GetComponent<Text>().text = "Press G to unlock the next area for 5 treasures\n\nIf you have the gem, press G to win!.";
        playerController.ResetLanternBrightness();
        UIController.Instance.unlockDoorIndicator.SetActive(true);
        shopOpen = true;
    }

    public void CloseShop()
    {
        UIController.Instance.unlockDoorIndicator.SetActive(false);
        shopOpen = false;
    }
}
