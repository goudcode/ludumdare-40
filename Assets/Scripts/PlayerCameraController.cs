﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraController : MonoBehaviour
{
    public float smoothness;
    private GameObject target;
    private GameObject player;
    public GameObject originPoint;
    private Vector2 velocityVector;

    public bool horizontal = true;
    public bool vertical = true;
    public bool stayOnOrigin = true;

    void Start()
    { }

    void Update()
    {
        if ((player == null && !FindAndSetPlayer()) || originPoint == null) // Camera can only update when it has a target.
            return;

        if (player.transform.position.y > -2)
        {
            stayOnOrigin = true;
            Camera.main.orthographicSize = 7;
        }
        else
        {
            Camera.main.orthographicSize = 7;
            stayOnOrigin = false;
        }

        var target = stayOnOrigin ? originPoint : player;
        // Calculating new positions for the camera
        float newPositionX = vertical ?
            Mathf.SmoothDamp(
            transform.position.x,
            target.transform.position.x,
            ref velocityVector.x, smoothness) : transform.position.x;

        float newPositionY = horizontal ? 
            Mathf.SmoothDamp(
            transform.position.y,
            target.transform.position.y,
            ref velocityVector.y, smoothness) : transform.position.y;
        
        transform.position = new Vector3(newPositionX, newPositionY, transform.position.z);
    }

    /// <summary>
    /// Finds the first GameObject with the tag player.
    /// If the GameObject is found the variable player
    ///     will point to that GameObject.
    /// </summary>
    /// <returns>Whether the GameObject with tag player is found or not</returns>
    private bool FindAndSetPlayer()
    {
        var player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
            this.player = player;

        return player != null;
    }
}
