﻿using UnityEngine;

[System.Serializable]
public class GameLayer
{
    public float minY;
    public float maxY;
    public AudioClip backgroundClip;
    public bool resetPlayerHealth;
}
