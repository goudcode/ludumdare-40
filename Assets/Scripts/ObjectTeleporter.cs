﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObjectTeleporter : MonoBehaviour
{
    public BoxCollider2D startZone;
    public GameObject particles;

    // Use this for initialization
    void Start()
    {    
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        collision.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        var newPos = new Vector3(
            Random.Range(-startZone.bounds.extents.x, startZone.bounds.extents.x),
            Random.Range(-startZone.bounds.extents.y, startZone.bounds.extents.y),
            0f) + startZone.transform.position;

        collision.gameObject.GetComponent<Rigidbody2D>().MovePosition(new Vector2(
            newPos.x, newPos.y
        ));
    }
}
