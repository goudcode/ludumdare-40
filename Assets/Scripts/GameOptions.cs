﻿using UnityEngine;

[System.Serializable]
public class GameOptions
{
    public float lanternBrightness;
    public float treasureWeight;
}
