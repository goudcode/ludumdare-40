﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    public GameObject lockedDoorIndicatorPrefab;
    public GameObject unlockDoorIndicatorPrefab;
    public Slider slider;
    [HideInInspector]
    public GameObject lockedDoorIndicator;
    [HideInInspector]
    public GameObject unlockDoorIndicator;

    public static UIController Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else Destroy(gameObject);
    }

    void Start()
    {
        if (lockedDoorIndicator == null)
        {
            lockedDoorIndicator = Instantiate(lockedDoorIndicatorPrefab, transform);
        }
        if (unlockDoorIndicator == null)
        {
            unlockDoorIndicator = Instantiate(unlockDoorIndicatorPrefab, transform);
        }
    }

    public void UpdateHealth(int current, int max)
    {
        slider.value = (float)current / max;
    }
}
