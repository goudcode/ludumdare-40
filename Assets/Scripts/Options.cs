﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour
{
    public Slider masterVolumeSlider;
    public Text masterVolumePercent;

    private void Start()
    {
        AudioListener.volume = 0.25f;
        masterVolumePercent.text = $"Volume: {(masterVolumeSlider.value * 100).ToString("F0")}%";
        masterVolumeSlider.onValueChanged.AddListener(ChangeMasterVolume);

    }
    public void ChangeMasterVolume(float value)
    {
        AudioListener.volume = value;
        masterVolumePercent.text = $"Volume: {(masterVolumeSlider.value * 100).ToString("F0")}%";
    }
}
