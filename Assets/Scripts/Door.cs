﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Door : MonoBehaviour
{
    public GameObject checkpoint;
    public GameObject enterDoorIndicatorPrefab;
    public Sprite openDoorSprite;
    public Door otherDoor;

    public bool isLocked;
    public int doorNumber;

    private GameObject enterDoorIndicator;
    private bool playerInDoor = false;
    private GameObject player;

    private void Start()
    {
        player = FindObjectOfType<PlayerController>().gameObject;

        enterDoorIndicator = Instantiate(enterDoorIndicatorPrefab,
            new Vector2(transform.position.x, transform.position.y + 2),
            Quaternion.identity, transform);
        enterDoorIndicator.SetActive(false);
        enterDoorIndicator.transform.localScale = Vector3.one;
    }

    private void Update()
    {
        if (playerInDoor)
        {
            UIController.Instance.lockedDoorIndicator.transform.position =
                Camera.main.WorldToScreenPoint(transform.position + new Vector3(0, 1, 0));
            if (isLocked)
            {
                UIController.Instance.lockedDoorIndicator.SetActive(true);
                enterDoorIndicator.SetActive(false);
            }
            else
            {
                UIController.Instance.lockedDoorIndicator.SetActive(false);
                enterDoorIndicator.SetActive(true);
                if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
                {
                    if(doorNumber == 0)
                    {
                        var unlockedDoors = FindObjectsOfType<Door>().Where(x => x.isLocked == false);
                        var furthestUnlockedDoor = unlockedDoors.OrderByDescending(x => x.doorNumber).First();

                        GameManager.Instance.SetCheckpoint(furthestUnlockedDoor);
                        player.transform.position = furthestUnlockedDoor.transform.position;
                    }
                    else
                    {
                        var startDoor = FindObjectsOfType<Door>().Where(x => x.doorNumber == 0).First();
                        player.transform.position = startDoor.transform.position;
                    }
                    playerInDoor = false;
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            UIController.Instance.lockedDoorIndicator.transform.position = Camera.main.WorldToScreenPoint(transform.position + new Vector3(0, 1, 0));
            playerInDoor = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            UIController.Instance.lockedDoorIndicator.SetActive(false);
            playerInDoor = false;
            enterDoorIndicator.SetActive(false);
        }
    }

    public void OpenDoor()
    {
        GetComponent<SpriteRenderer>().sprite = openDoorSprite;
        isLocked = false;
    }
}
