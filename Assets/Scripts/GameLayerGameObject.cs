﻿using UnityEngine;

public class GameLayerGameObject : MonoBehaviour
{
    public GameLayer layer;
    public GameManager gameManager;

    private void Start()
    {
        transform.position = new Vector3(0, (layer.minY + layer.maxY) / 2, 0);
        GetComponent<BoxCollider2D>().size = new Vector2(500, Mathf.Abs(layer.maxY - layer.minY));
        gameManager = FindObjectOfType<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            gameManager.SetBackgroundMusic(layer.backgroundClip);
            if (layer.resetPlayerHealth)
                gameManager.ResetPlayerHealth();
        }
    }
}