﻿using UnityEngine;

public class Lava : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
            FindObjectOfType<PlayerController>().Die();

        if (collision.gameObject.tag == "Treasure")
        {
            var force = new Vector2(Random.Range(-25f, 25f), 10f) * 10;
            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(force);
        }
    }
}