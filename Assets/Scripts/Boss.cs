﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public enum BossState
    {
        Idle,
        Attack,
    }

    public int health;
    public AudioClip shootingSound;
    public AudioClip[] hurtSounds;
    private BossState state;
    private Animator animator;
    private AudioSource audioSource;
    private GameObject player;
    public Transform projectileOrigin;
    private bool facingRight = false;

    public GameObject projectile;
    public GameObject gemPrefab;
    public GameObject gemSpawn;
    public float attackSpeed = 3;
    private float attackTime;
    private Vector2 playerDirection;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        attackTime = attackSpeed;
    }

    void Update()
    {
        attackTime -= Time.deltaTime;

        if(player.transform.position.x < transform.position.x && facingRight)
        {
            Flip();
        }
        else if(player.transform.position.x > transform.position.x && !facingRight)
        {
            Flip();
        }

        playerDirection = player.transform.position - projectileOrigin.position;
        switch (state)
        {
            case BossState.Idle:
                OnIdle();
                break;

            case BossState.Attack:
                OnAttack();
                break;
        }
    }

    private void OnAttack()
    {
        if (!PlayerInSight())
        {
            state = BossState.Idle;
            return;
        }
        if(attackTime <= 0)
        {
            animator.SetBool("Attacking", true);
        }
    }

    void OnIdle()
    {
        if (!PlayerInSight())
            return;

        state = BossState.Attack;
    }

    private bool PlayerInSight()
    {
        //Find player
        if (player == null)
            return false;

        //See if player is in sight
        RaycastHit2D hit = Physics2D.Linecast(transform.position, player.transform.position,
            ~(1 << gameObject.layer));
        if (hit.collider?.tag == "Player")
            return true;

        return false;
    }

    void Attack()
    {
        Instantiate(projectile, projectileOrigin.position, Quaternion.identity).GetComponent<Projectile>().SetDirection(playerDirection); ;
        audioSource.PlayOneShot(shootingSound);
        attackTime = attackSpeed;
        animator.SetBool("Attacking", false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Treasure"))
        {
            TakeDamage();
            audioSource.PlayOneShot(hurtSounds[UnityEngine.Random.Range(0, hurtSounds.Length)]);
        }
    }

    private void Flip()
    {
        facingRight = !facingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }

    private void TakeDamage()
    {
        health--;
        if(health <= 0)
        {
            animator.SetBool("Dying", true);
        }
    }
    
    private void Destroy()
    {
        Instantiate(gemPrefab, gemSpawn.transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
