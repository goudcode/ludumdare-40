﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    private AudioSource audioPlayer;

    public GameOptions options;
    public GameObject gameLayerPrefab;
    public GameLayer[] gameLayers;
    public PlayerController player;
    public Vector2 lastCheckpoint;
    public Door lastDoor;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else Destroy(gameObject);
    }

    private void Start()
    {
        audioPlayer = GetComponent<AudioSource>();
        player = FindObjectOfType<PlayerController>();
        lastCheckpoint = player.transform.position;

        foreach (GameLayer layer in gameLayers)
        {
            GameLayerGameObject go = Instantiate(gameLayerPrefab, transform).GetComponent<GameLayerGameObject>();
            go.layer = layer;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
            AudioListener.volume = 0;
    }

    private void OnDrawGizmosSelected()
    {
        foreach (GameLayer layer in gameLayers)
        {
            Gizmos.DrawWireCube(new Vector3(0, (layer.minY + layer.maxY) / 2, 0), new Vector2(500, Mathf.Abs(layer.maxY - layer.minY)));
        }
    }

    public void SetBackgroundMusic(AudioClip clip)
    {
        if(audioPlayer.clip != clip)
        {
            audioPlayer.clip = clip;
            audioPlayer.Play();
        }
    }

    public void ResetPlayerHealth()
    {
        player.ResetHealth();
    }

    public void SetCheckpoint(Door lastDoor)
    {
        lastCheckpoint = lastDoor.transform.position;
        this.lastDoor = lastDoor;
    }
}
